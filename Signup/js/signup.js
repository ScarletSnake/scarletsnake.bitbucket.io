const url = 'https://localhost:5001/api/registration';

document.getElementById("postUser").addEventListener("click", createUser);

function createUser() {
  var user = {};
  user.login = document.getElementById('before_login').value;
  user.email = document.getElementById('before_email').value;
  user.password = document.getElementById('before_password').value;
  user.phone = document.getElementById('before_phone').value;
  user.country = document.getElementById('before_country').value;

  try {
    fetch(url, {
      // mode: no-cors,
      method: 'POST',
      headers: {
        // 'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    })
      .catch((e) => { console.log(e) })
    // .then(response => {console.log(response)}).then(window.location.replace("../Consumptions/consumptions.html"));
  }
  catch (error) {
    console.error('Ошибка:', error);
  }
}